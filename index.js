const http = require('http');
const express = require('express');
const request = require('request');
const app = express();
const { pin } = require('./pin');
const DatGateway = require('./gateway');
const dotenv = require('dotenv');
dotenv.config();

const slackBotPort = process.env.SLACK_BOT_PORT || 3000;
const { createEventAdapter } = require('@slack/events-api');

const slackEvents = createEventAdapter(process.env.SLACK_SIGNING_SECRET);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.use('/slack/events', slackEvents.expressMiddleware());

// Attach listeners to events by Slack Event "type". See: https://api.slack.com/events/message.im
slackEvents.on('message', (event) => {

    //console.log(`Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`);

    if (event.text.includes(process.env.BOT_NAME)) {

        if (event.text.includes('pin')) {

            pin(event).then((responseMsg) => {

                const postMessageUrl = `http://slack.com/api/chat.postMessage?token=${process.env.OAUTH_ACCESS_TOKEN}&channel=${process.env.SLACK_CHANNEL}&text=${responseMsg}`;

                request(postMessageUrl, (err, response, body) => {
                    if (!err && response.statusCode === 200) { 
                        console.log('Successful request and response to Fusebot');
                    } else { 
                        console.log(err);
                    }
                });

            }).catch((err) => {
                console.log(err);
            });
        }

    }
});

// Handle errors (see `errorCodes` export)
slackEvents.on('error', console.error);

// Start the express application
http.createServer(app).listen(slackBotPort, () => {
    console.log(`slack bot listening on port ${slackBotPort}`);
});

const host = '0.0.0.0';

const datGatewayPort = process.env.DAT_GATEWAY_PORT;

// the directory to store/retrieve dats to/from
const dir = process.env.DAT_DIRECTORY;
// Maximum number of archives to serve at a time
const max = process.env.GATEWAY_MAX_ARCHIVES_CACHED;
// Number of milliseconds before archives are removed from the cache
const maxAge = process.env.GATEWAY_MAX_CACHED_AGE;

const gateway = new DatGateway({ dir, max, maxAge });

gateway.listen(datGatewayPort, host).then(() => {
    console.log('dat-gateway now listening on ' + host + ':' + datGatewayPort);
}).catch(console.error);
