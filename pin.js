const Dat = require('dat-node');
const fs = require('fs');

exports.pin = (event) => {

    return new Promise((resolve, reject) => {

        // todo: remove anything after the dat key, like /index.json
        const key = event.text.replace(process.env.BOT_NAME, '').replace('pin', '').replace('dat://', '').replace('<', '').replace('>', '').trim();

        // download dat            
        const datdir = process.env.DAT_DIRECTORY;

        if (!fs.existsSync(datdir)) {
            fs.mkdirSync(datdir);
        }

        const keydir = process.env.DAT_DIRECTORY + '/' + key;

        if (!fs.existsSync(keydir)) {

            fs.mkdirSync(keydir);

            Dat(keydir, {
                key: key 
            }, (err, dat) => {
                if (err) {
                    return reject(err);
                } else {
                    dat.archive.on('sync', () => {
                        return resolve(`<@${event.user}> I successfully pinned dat://${key}`);
                    });

                    dat.archive.on('error', () => {
                        return resolve(`<@${event.user}> There was an error when pinning dat://${key}`);
                    });

                    dat.joinNetwork((err) => {
                        if (err) {
                            return reject(err);
                        }
                    });
                }            
            });
        } else {
            return resolve(`<@${event.user}> I've already pinned dat://${key}`);
            // todo: sync the dat?
        }
    }).catch((err) => {
        return reject(err);
    });
}