# fusebot

A slack bot for preserving and sharing Dats via an http gateway.

Only Dats pinned by fusebot are available via the http gateway.

    @fusebot <dat-key>

To use with uv.pub locally, enter `http://localhost:3001` as the http gateway (port specified in .env).

Run in debug to access .env settings (F5).

## ubuntu commands

Find the process if it's running:

    ps aux | grep index.js

Grab the number from the second column and run:

    sudo kill -9 <pid>

To start the daemonised server, cd to the fusebot dir and run:

    sudo nohup node index.js &

To tunnel install localtunnel:

    sudo npm install -g localtunnel

Then run:

    sudo nohup npm run tunnel &

<!--
## test

Make sure laptop is on the Digital Catapult - Brighton WiFi network

https://github/edsiv/biiif-test-manifests (gh-pages branch)

Run https://github.com/universalviewer/uv.pub

    npm start

Enter 5G server dat gateway address:

    http://10.0.0.112:3001

-->